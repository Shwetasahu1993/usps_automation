package generalPractice;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class USPScom {
	WebDriver driver;
	Actions actions;

	@BeforeSuite
	public void init() {
		System.setProperty("webdriver.gecko.driver", "/Users/shwetagupta/Desktop/geckodriver");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		actions = new Actions(driver);
		//test comments1
	}

	@Test
	public void openUSPSTest() {
		driver.get("https://www.usps.com/");
	}

	@Test(dependsOnMethods = "openUSPSTest")
	public void loginUSPSTest() {
		driver.findElement(By.id("login-register-header")).click();

		driver.findElement(By.id("username")).sendKeys("shwetasahu1993@gmail.com");

		driver.findElement(By.id("password")).sendKeys("S0mething@123");

		driver.findElement(By.id("btn-submit")).click();
	}

	@Test(dependsOnMethods = "loginUSPSTest")
	public void mouseOver() {

		actions.moveToElement(driver.findElement(By.linkText("Quick Tools"))).perform();

		actions.moveToElement(driver.findElement(By.linkText("Mail & Ship"))).perform();

		actions.moveToElement(driver.findElement(By.linkText("Track & Manage"))).perform();

		actions.moveToElement(driver.findElement(By.linkText("Postal Store"))).perform();

		actions.moveToElement(driver.findElement(By.linkText("Business"))).perform();

		actions.moveToElement(driver.findElement(By.linkText("International"))).perform();

		actions.moveToElement(driver.findElement(By.linkText("Help"))).perform();

	}

	@Test(dependsOnMethods = "mouseOver")
	public void searchBox() throws InterruptedException {

		actions.moveToElement(driver.findElement(By.linkText("Search USPS.com"))).perform();
		//actions.moveToElement(driver.findElement(By.name("q"))).sendKeys("po boxes").click();
		//actions.moveToElement(driver.findElement(By.id("global-header--search-track-mob-search"))).sendKeys("po boxes");
		driver.findElement(By.name("q")).sendKeys("Po");
		driver.findElement(By.name("q")).sendKeys(Keys.ENTER);
		Thread.sleep(3000);
		
		driver.findElement(By.linkText("PO Boxes | USPS")).click();
		Thread.sleep(3000);
		driver.navigate().back();
		Thread.sleep(3000);


	}

	
	@AfterSuite
	public void teardown() {
		System.out.println("In AfterSuite, Execution is completed ");
		driver.close();

	}

}